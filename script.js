function validateInput() {
    let username = document.getElementById("username").value;
    let email = document.getElementById("inputEmail").value;
    let confirmEmail = document.getElementById("inputEmailConfirm").value;
    let password = document.getElementById("inputPassword").value;
    let confirmPassword = document.getElementById("inputPasswordConfirm").value;

    if (!username || !email || ! confirmEmail || !password || !confirmPassword) {
        alert("All field must be filled");
        return false;
    }

    if (!username.match(/.{3}/)) {
        alert("Username must be 3 character minimum");
        return false;
    }


    const regexEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!email.match(regexEmail)) {
        alert("Email must be appropriate");
        return false;
    }

    if (!password.match(/.{6}/)) {
        alert("Password must be 6 character minimum");
        return false;
    }

    if (email != confirmEmail) {
        alert("Email and confirm email must be same");
        return false;
    }

    if (password != confirmPassword) {
        alert("Password and confirm password must be same");
        return false;
    }

    return true;
}

document.body.querySelector('#submitButton').addEventListener('click', () => {
    if (validateInput()) {
        document.getElementById("username").value = "";
        document.getElementById("inputEmail").value = "";
        document.getElementById("inputEmailConfirm").value = "";
        document.getElementById("inputPassword").value = "";
        document.getElementById("inputPasswordConfirm").value = "";
        alert("Registration Successful");
    }
});